# CLIMATEGRAPHS.INFO #

Map of the climategraphs from all over the world. Data from Netatmo API.

### Setup ###

* you'll need nodeJS and MySQL
* npm install
* create config.json file based on config.json.example
* import schema.sql
* node server.js

### Contact ###

* wkoszyk at gmail dot com