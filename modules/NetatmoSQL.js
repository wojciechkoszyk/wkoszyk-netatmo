var mysql = require('mysql');
var async = require('async');

function NetatmoSQL(options) {
	this.mysqlClient = mysql.createConnection(options.nconf.get('mysql'));
};

NetatmoSQL.prototype.saveTokens = function(tokens, callback) {
	this.mysqlClient.query('UPDATE oauth_tokens SET ? WHERE id = 1', {
		access_token: tokens.access_token,
		refresh_token: tokens.refresh_token
	}, function(err, result) {
	    if (err) throw err;
	    callback();
	});
};

NetatmoSQL.prototype.getTokens = function(callback) {
	this.mysqlClient.query('SELECT access_token, refresh_token FROM oauth_tokens WHERE id = 1', function(err, result) {
	    if (err) throw err;
	    callback(result);
	});
};

NetatmoSQL.prototype.savePublicDevice = function(device, callback) {
	var self = this;

	this.mysqlClient.query('UPDATE public SET ? WHERE id = '+this.mysqlClient.escape(device.id), {
		lat: parseFloat(device.lat).toFixed(8),
		lon: parseFloat(device.lon).toFixed(8),
		alt: device.alt,
		modules: device.modules,
		mark: device.mark
	}, function(err, result) {
	    if (err) throw err;
	    if(result.affectedRows === 0) {
	        self.mysqlClient.query('INSERT INTO public SET ? ', {
	        	id: device.id, 
	        	lat: parseFloat(device.lat).toFixed(8),
	        	lon: parseFloat(device.lon).toFixed(8),
	        	alt: device.alt,
	        	modules: device.modules,
	        	mark: device.mark,
	        	zoom: device.zoom
	        }, function(err, result) {
	            if (err) throw err;
	        	
	        	callback(null,'');
	        });
	    } else {
	        callback(null,'');
	    }
	});
};

NetatmoSQL.prototype.getPublicDevices = function(callback) {
	this.mysqlClient.query('SELECT * FROM public', function(err, result) {
		if(typeof callback === 'function') {
			callback(result);
		}
	});
};

NetatmoSQL.prototype.getPublicDeviceModules = function(deviceId, callback) {
	this.mysqlClient.query('SELECT modules FROM public WHERE id = '+this.mysqlClient.escape(deviceId), function(err, result) {
		if(typeof callback === 'function') {
			callback(result);
		}
	});
};

NetatmoSQL.prototype.getBoundsDevices = function(bounds, callback) {
	var latMin = bounds._northEast.lat > bounds._southWest.lat ? bounds._southWest.lat : bounds._northEast.lat;
	var latMax = bounds._northEast.lat > bounds._southWest.lat ? bounds._northEast.lat : bounds._southWest.lat;
	var lonMin = bounds._northEast.lng > bounds._southWest.lng ? bounds._southWest.lng : bounds._northEast.lng;
	var lonMax = bounds._northEast.lng > bounds._southWest.lng ? bounds._northEast.lng : bounds._southWest.lng;

	var query = 'SELECT * FROM public WHERE lat BETWEEN '+latMin+' AND '+latMax+' AND lon BETWEEN '+lonMin+' AND '+lonMax+' AND mark > 5 AND mark IS NOT NULL AND zoom <='+bounds.zoom;

	this.mysqlClient.query(query, function(err, result) {
		if(typeof callback === 'function') {
			callback(result);
		}
	});
};

NetatmoSQL.prototype.getClimatograph = function(options, callback) {
	var query = 'SELECT * FROM climatographs WHERE device_id = "'+options.deviceId+'" AND year = "'+options.selectedYear+'"';

	this.mysqlClient.query(query, function(err, result) {
		if(typeof callback === 'function') {
			callback(result);
		}
	});
};

NetatmoSQL.prototype.saveClimatograph = function(data, callback) {
	var self = this;

	this.mysqlClient.query('INSERT INTO climatographs SET ?', {
		device_id: data.deviceId,
		data: JSON.stringify(data.climatographData),
		range: JSON.stringify(data.readingsRange),
		year: data.selectedYear
	}, function(err, result) {
		console.log('saveClimatograph error:', JSON.stringify(err));
		console.log('saveClimatograph result:', JSON.stringify(result));

		if(err) {
			self.updateClimatograph(data, callback);
		} else {
			if(typeof callback === 'function') {
				callback();
			}
		}
	});
};

NetatmoSQL.prototype.updateClimatograph = function(data, callback) {
	this.mysqlClient.query('UPDATE climatographs SET ? WHERE device_id = '+this.mysqlClient.escape(data.deviceId)+' AND year = '+this.mysqlClient.escape(data.selectedYear), {
		data: JSON.stringify(data.climatographData),
		range: JSON.stringify(data.readingsRange)
	}, function(err, result) {
		console.log('updateClimatograph error:', JSON.stringify(err));
		console.log('updateClimatograph result:', JSON.stringify(result));
		if(typeof callback === 'function') {
			callback();
		}
	});
};

NetatmoSQL.prototype.saveCity = function(data, callback) {
	console.log('saveCity', JSON.stringify(data));
	this.mysqlClient.query('UPDATE climatographs SET ? WHERE device_id = '+this.mysqlClient.escape(data.deviceId), {
		city: data.city,
		country: data.country
	}, function(err, result) {
		if(err !== null || result.affectedRows === 0) {
			console.log(err);
			callback({error:true});			
		} else {
			console.log('saveCity result:', JSON.stringify(result));
	        callback({});
		}
	});
};

module.exports = NetatmoSQL;