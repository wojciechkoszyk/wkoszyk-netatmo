var querystring = require('querystring');
var https = require('https');
var async = require('async');
var NetatmoClimatograph = require(__dirname + '/NetatmoClimatograph.js');

function NetatmoApi(options) {
	this.mysql = options.mysql;
	this.oauth = options.oauth;
	this.attempts = 0;
};

NetatmoApi.prototype.makeApiCall = function(options) {
	var self = this;

	this.mysql.getTokens(function(result) {
		options.data.access_token = result[0].access_token;

		var post_data = querystring.stringify(options.data);

		var post_options = {
		    host: 'api.netatmo.net',
		    port: '443',
		    path: '/api/'+options.path,
		    method: 'POST',
		    headers: {
		        'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
		        'Content-Length': Buffer.byteLength(post_data)
		    }
		};

		// Set up the request
		var post_req = https.request(post_options, function(res, err) {
		    var data = '';

		    res.setEncoding('utf8');
		    res.on('data', function (chunk) {
		        data += chunk;
		    });

		    res.on('end', function() {
		        data = JSON.parse(data);

		        if(!data.error && typeof options.apiSuccess === 'function') {
		        	console.log('makeApiCall success')
		        	options.apiSuccess(data);
		        }

		        if(data.error) {
		        	if(self.attempts < 3) {
		        		console.log('MAKE API CALL', typeof self.oauth.authorizeFromCredentials);
		        		self.oauth.authorizeFromCredentials();
		        		setTimeout(function() {
			        		self.attempts++;
			        		self.makeApiCall(options);
		        		}, 3000);
		        	} else if (typeof options.apiError === 'function') {
		        		console.log('makeApiCall error', data);
		        		options.apiError(data);
		        	} else {
		        		console.log('makeApiCall error which cant be displayed', data);
		        	}
		        }

		        post_req.end();
		    });
		});

		post_req.write(post_data);
	});
};

NetatmoApi.prototype.preparePublicDevices = function(data) {
	var devices = [];

	for(var i=0; i<data.body.length; i++) {
		var device = data.body[i];
		var toSave = {
			id: device._id,
			lat: device.place.location[1],
			lon: device.place.location[0],
			alt: device.place.altitude,
			modules: JSON.stringify(device.modules),
			mark: device.mark
		};
		devices.push(toSave);
	}

	return devices;
};

NetatmoApi.prototype.updatePublicDeviceCallback = function(deviceData) {
	var self = this;

	return function(callback) {
		self.mysql.savePublicDevice(deviceData, callback);
	}
};

NetatmoApi.prototype.getPublicDevices = function(options, callback) {
	var self = this;

	var data = {
		lat_ne: options.data.lat_ne,
		lon_ne: options.data.lon_ne,
		lat_sw: options.data.lat_sw,
		lon_sw: options.data.lon_sw
	};

	this.makeApiCall({
		path: 'getpublicdata',
		data: data,
		apiSuccess: function(data) {
			console.log('api getPublicDevices apiSuccess')
			console.log('data', data.body.length);

			var devices = self.preparePublicDevices(data);
			var devicesCallbacks = [];

			for(var i=0; i<devices.length; i++) {
				devices[i].zoom = options.data.zoom;
				var devicesCallbackReturn = self.updatePublicDeviceCallback(devices[i]);
				devicesCallbacks.push(devicesCallbackReturn);
			}

			async.series(devicesCallbacks, function() {
			    if(typeof options.onSuccess === 'function') {
			    	options.onSuccess({});
			    }
			});
		},
		apiError: function(error) {
			console.log('api getPublicDevices apiError')
			if(typeof options.onError === 'function') {
				options.onError(error);
			}
		}
	});
};

NetatmoApi.prototype.getClimatograph = function(options, mainCallback) {
	var self = this;

	this.mysql.getPublicDeviceModules(options.deviceId, function(modules) {
		modules = JSON.parse(modules[0].modules);
		var measureCallbacks = [];
		var range = self.getRangeFromYear(options.selectedYear);

		for(var i=0; i<modules.length; i++) {
			var type = i === 0 ? 'min_temp,max_temp' : 'sum_rain';
			var data = {
				device_id: options.deviceId,
				module_id: modules[i],
				scale: '1day',
				type: type,
				optimize: false,
				date_begin: range.from, 
				date_end: range.to
			};
			var measureCallbackReturn = self.getMeasureCallback(data);
			measureCallbacks.push(measureCallbackReturn);
		}

		async.series(measureCallbacks, function(err, result) {
			for(var i=0; i<result.length; i++) {
				if(result[i].error === true) {
					mainCallback({});
				}
			}

			var dataToAnalyse = [];

			for(var i=0; i<result.length; i++) {
				dataToAnalyse.push(result[i].body);
			}

			var climatograph = new NetatmoClimatograph();
			var clData = {
				deviceId: data.device_id,
				climatographData: climatograph.getData(dataToAnalyse),
				readingsRange: climatograph.getReadingsRange(),
				selectedYear: options.selectedYear
			};

			if(typeof options.update !== 'undefined' && options.update === true) {
				self.mysql.updateClimatograph(clData, function() {
					if(typeof mainCallback === 'function') {
						mainCallback(clData);
					}
				});
			} else {
				self.mysql.saveClimatograph(clData, function() {
					if(typeof mainCallback === 'function') {
						mainCallback(clData);
					}
				});
			}
		});
	});
};

NetatmoApi.prototype.getMeasureCallback = function(data) {
	var self = this;

	return function(callback) {
		self.getMeasure(data, callback);
	}
};

NetatmoApi.prototype.getMeasure = function(data, callback) {
	var self = this;

	this.makeApiCall({
		path: 'getmeasure',
		data: data,
		apiSuccess: function(data) {
			console.log('getClimatograph success, retrieved', Object.keys(data.body).length, 'days');
			// if(Object.keys(data.body) === 0) {
				// data = {error:true};
			// }
			callback(null, data);
		},
		apiError: function(error) {
			console.log('getClimatograph error', error);
			callback(null, {error:true});
		}
	});
};

NetatmoApi.prototype.getRangeFromYear = function(year) {
	var range = {};
	range.from = new Date(year, 0, 0, 1, 0, 1).getTime() / 1000;
	range.to = new Date(year, 11, 30, 22, 59, 0).getTime() / 1000;

	return range;
};


module.exports = NetatmoApi;