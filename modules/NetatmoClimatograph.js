function NetatmoClimatograph() {
	this.readingsRange = {};
	this.availableData = {
		temperature: false,
		rain: false
	};

	this.minimalPrecipitation = 1;
};

NetatmoClimatograph.prototype.sortObjectByKey = function(obj) {
    var keys = Object.keys(obj);
    var sorted = [];

    keys.sort(function(a, b) {
        return parseInt(a) - parseInt(b);
    });

    for(var i=0; i<keys.length; i++) {
    	var object = {
    		timestamp: keys[i],
    		data: obj[keys[i]]
    	};
        sorted.push(object);
    }

    return sorted;
};

NetatmoClimatograph.prototype.analyseData = function(data) {
	var climatograph = {};

	if(data.length === 0 || data[0].length === 0) {
		return null;
	}

	for(var k=0; k<data.length; k++) {
		var sortedData = this.sortObjectByKey(data[k]);

		if(k === 0) {
			this.analyseOutdoor(sortedData, climatograph);
			this.availableData.temperature = true;
		} else if (k === 1) {
			this.analyseRain(sortedData, climatograph);
			this.availableData.rain = true;
		}
	}

	return this.makeClimatograph(climatograph);
};

NetatmoClimatograph.prototype.analyseOutdoor = function(sortedData, climatograph) {
	this.readingsRange.outdoor = {};

	for(var i=0; i<sortedData.length; i++) {
		var measurement = sortedData[i];

		var date = new Date(measurement.timestamp*1000);
		var month = date.getMonth();
		var day = date.getDate();

		if(i===0) {
			this.readingsRange.outdoor.from = this.parseDate(measurement.timestamp);
		}
		if(i===sortedData.length-1) {
			this.readingsRange.outdoor.to = this.parseDate(measurement.timestamp);
		}

		if(typeof climatograph[month] === 'undefined') {
			climatograph[month] = {};
		}

		if(typeof climatograph[month][day] === 'undefined') {
			climatograph[month][day] = {
				temperatureMin: measurement.data[0],
				temperatureMax: measurement.data[1],
				temperatureAvg: (measurement.data[0]+measurement.data[1]) / 2
			};
		}
	}
};

NetatmoClimatograph.prototype.analyseRain = function(sortedData, climatograph) {
	this.readingsRange.rain = {};

	for(var i=0; i<sortedData.length; i++) {
		var measurement = sortedData[i];

		var date = new Date(measurement.timestamp*1000);
		var month = date.getMonth();
		var day = date.getDate();

		if(i===0) {
			this.readingsRange.rain.from = this.parseDate(measurement.timestamp);
		}
		if(i===sortedData.length-1) {
			this.readingsRange.rain.to = this.parseDate(measurement.timestamp);
		}

		if(typeof climatograph[month] === 'undefined') {
			climatograph[month] = {};
		}

		if(typeof climatograph[month][day] === 'undefined') {
			climatograph[month][day] = {
				rain: measurement.data[0]
			}
		} else {
			climatograph[month][day].rain = measurement.data[0];
		}
	}
};

NetatmoClimatograph.prototype.makeClimatograph = function(data) {
	var climatographData = {};

	for(var k=0; k<12; k++) {
		climatographData[k] = {
			temperatureRmax: '-',
			temperatureMax: '-',
			temperatureMin: '-',
			temperatureRmin: '-',
			temperatureAvg: '-',
			temperatureIce: '-',
			temperatureFrost: '-',
			temperatureCold: '-',
			temperatureSummer: '-',
			temperatureHot: '-',
			rain: '-',
			rainDays: '-'
		};
	}

	for(var i in data) {
		var days = Object.keys(data[i]).length;

		for(var j in data[i]) {
			// max temperature, prepare to get average
			if(isNaN(climatographData[i].temperatureMax)) {
				climatographData[i].temperatureMax = 0;
			}
			climatographData[i].temperatureMax += data[i][j].temperatureMax;

			// min temperature, prepare to get average
			if(isNaN(climatographData[i].temperatureMin)) {
				climatographData[i].temperatureMin = 0;
			}
			climatographData[i].temperatureMin += data[i][j].temperatureMin;

			// record max
			if(isNaN(climatographData[i].temperatureRmin)) {
				climatographData[i].temperatureRmax = -100;
			}
			if(data[i][j].temperatureMax > climatographData[i].temperatureRmax) {
				climatographData[i].temperatureRmax = data[i][j].temperatureMax;
			}

			// record low
			if(isNaN(climatographData[i].temperatureRmin)) {
				climatographData[i].temperatureRmin = 100;
			}
			if(data[i][j].temperatureMin < climatographData[i].temperatureRmin) {
				climatographData[i].temperatureRmin = data[i][j].temperatureMin;
			}

			// average daily temperature
			if(isNaN(climatographData[i].temperatureAvg)) {
				climatographData[i].temperatureAvg = 0;
			}
			climatographData[i].temperatureAvg += data[i][j].temperatureAvg;

			// ice temp
			if(isNaN(climatographData[i].temperatureIce)) {
				climatographData[i].temperatureIce = 0;
			}
			if(data[i][j].temperatureMax < 0) {
				climatographData[i].temperatureIce++;
			}

			// frost temp
			if(isNaN(climatographData[i].temperatureFrost)) {
				climatographData[i].temperatureFrost = 0;
			}
			if(data[i][j].temperatureMin < 0) {
				climatographData[i].temperatureFrost++;
			}

			// cold days 
			if(isNaN(climatographData[i].temperatureCold)) {
				climatographData[i].temperatureCold = 0;
			}
			if(data[i][j].temperatureMax < 10) {
				climatographData[i].temperatureCold++;
			}

			// summer days 
			if(isNaN(climatographData[i].temperatureSummer)) {
				climatographData[i].temperatureSummer = 0;
			}
			if(data[i][j].temperatureMax >= 25) {
				climatographData[i].temperatureSummer++;
			}

			// hot days 
			if(isNaN(climatographData[i].temperatureHot)) {
				climatographData[i].temperatureHot = 0;
			}
			if(data[i][j].temperatureMax >= 30) {
				climatographData[i].temperatureHot++;
			}

			// rain
			if(isNaN(climatographData[i].rain)) {
				climatographData[i].rain = 0;
				climatographData[i].rainDays = 0;
			}
			climatographData[i].rain += data[i][j].rain;
			if(data[i][j].rain > this.minimalPrecipitation) {
				climatographData[i].rainDays++;
			}
		}

		climatographData[i].temperatureMax = (climatographData[i].temperatureMax / days).toFixed(1);
		climatographData[i].temperatureMin = (climatographData[i].temperatureMin / days).toFixed(1);
		climatographData[i].temperatureAvg = (climatographData[i].temperatureAvg / days).toFixed(1);
		climatographData[i].temperatureRmax = climatographData[i].temperatureRmax.toFixed(1);
		climatographData[i].temperatureRmin = climatographData[i].temperatureRmin.toFixed(1);
		if(isNaN(climatographData[i].rain)) {
			climatographData[i].rain = '-';
			climatographData[i].rainDays = '-';
		} else {
			climatographData[i].rain = climatographData[i].rain.toFixed(1);
		}
	}

	climatographData.sums = this.addSum(climatographData);

	return climatographData;
};

NetatmoClimatograph.prototype.addSum = function(data) {
	var monthsLength = 0;
	var sums = {
		temperatureRmax: 0,
		temperatureMax: 0,
		temperatureMin: 0,
		temperatureRmin: 0,
		temperatureAvg: 0,
		temperatureIce: 0,
		temperatureFrost: 0,
		temperatureCold: 0,
		temperatureSummer: 0,
		temperatureHot: 0,
		rain: 0,
		rainDays: 0
	};

	// sum all rows
	for(var i in data) {
		for(var j in data[i]) {
			if(!isNaN(data[i][j])) {
				sums[j] += parseFloat(data[i][j]);

				// count months with data
				if(j === 'temperatureMax') {
					monthsLength++;
				}
			}
		}
	}

	// get average
	for(var k in sums) {
		sums[k] = (sums[k]/monthsLength).toFixed(1);
 	}

	return sums;
};

NetatmoClimatograph.prototype.getData = function(data) {
	return this.analyseData(data);
};

NetatmoClimatograph.prototype.getReadingsRange = function() {
	return this.readingsRange;
};

NetatmoClimatograph.prototype.getAvailableData = function() {
	return this.availableData;
};

NetatmoClimatograph.prototype.parseDate = function(timestamp) {
	var date = new Date(timestamp*1000);

	var day = date.getDate();
	if(day < 10) {
		day = '0'+day;
	}

	var month = date.getMonth()+1;
	if(month < 10) {
		month = '0'+month;
	}

	var year = date.getFullYear();

	return day+'/'+month+'/'+year;
};
module.exports = NetatmoClimatograph;
