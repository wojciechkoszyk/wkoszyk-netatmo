var async = require('async');
var NetatmoApi = require(__dirname + '/NetatmoApi.js');

function NetatmoHistory(options) {
	this.mysqlClient = options.mysqlClient || {};
	this.session = options.session || {};
	this.netatmoApi = new NetatmoApi({
		mysql: this.mysqlClient
	});

	this.saved = 0;
};

NetatmoHistory.prototype.setToDatabase = function(options, callback) {
	var self = this;

	var mainCallbacks = [];
	var range = this.determineStartEndDates(options.month, options.year);

	this.mysqlClient.getDeviceInfo(options.device_id, function(deviceData) {
		var deviceId = deviceData.device_id !== null ? deviceData.device_id : deviceData.id;
		var moduleId = deviceData.device_id !== null ? deviceData.id : null;

		while(range.start < range.end) {
			var from = range.start.getTime()/1000;
			var to = range.start.setDate(range.start.getDate() + 1);

			var data = {
				id: deviceData.id,
				from: from,
				to: to/1000,
				deviceID: deviceId,
				moduleID: moduleId,
				dataType: deviceData.data_type,
				deviceType: deviceData.device_type
			};

		    var mainCallbackReturn = self.getNetatmoDataCallback(data);
		    mainCallbacks.push(mainCallbackReturn);

		    range.start = new Date(to);
		}

		async.series(mainCallbacks, function() {
		    console.log('mainDone', mainCallbacks.length);
		    if(typeof callback === 'function') {
		    	callback();
		    }
		});
	});
};

NetatmoHistory.prototype.determineStartEndDates = function(month, year) {
	var startEnd = {};

	var monthFrom = 0;
	var monthTo = 11;
	if(month !== '' && typeof month !== 'undefined') {
		monthFrom = month;
		monthTo = month;
	}

	if(parseInt(year) > 1900 && parseInt(year) < 2100) {
		startEnd.start = new Date(year,monthFrom,1,0,0,0);
		startEnd.end = new Date(year,monthTo,31,23,59,59);
	}

	return startEnd;
};

NetatmoHistory.prototype.getNetatmoDataCallback = function(options) {
	var self = this;

	return function(callback) {
	    self.getNetatmoData(options, callback);
	}
};

NetatmoHistory.prototype.getNetatmoData = function(reqOptions, mainCallback) {
	var self = this; 

	var options = {
		access_token: this.session.access_token,
	    device_id: reqOptions.deviceID,
	    module_id: reqOptions.moduleID,
	    scale: 'max',
	    date_begin: reqOptions.from,
	    date_end: reqOptions.to,
	    optimize: false,
	    type: JSON.parse(reqOptions.dataType).toString(),
	};

	this.netatmoApi.getMeasure(options, function(measure) {
	    var callbacks = [];
	    console.log('measure', Object.keys(measure.body).length);
	    for(var i in measure.body) {
	        var savedRowReturn = self.mysqlClient.setHistoryRow(i, measure.body[i], reqOptions.id);
	        callbacks.push(savedRowReturn);
	    }

	    async.series(callbacks, function() {
	        console.log('Saved', callbacks.length, 'rows');

	        // netatmo has limits 1024 rows at once
	        // so when limit is reached, wait 5s and grab another portion
	        if(self.saved + callbacks.length > 1024) {
	        	setTimeout(function() {
	        	    mainCallback(null, '');
	        	}, 5000);
	        	self.saved = 0;
	        } else {
	        	mainCallback(null, '');
	        }

	        self.saved += callbacks.length;
	    });
	});
};

NetatmoHistory.prototype.getDevicesInfo = function(devicesList, callback) {
	var self = this;
	var callbacks = [];

	for(var i=0; i<devicesList.length; i++) {
	    var getDevicesInfoReturn = this.mysqlClient.getDeviceMinMax(devicesList[i]);
	    callbacks.push(getDevicesInfoReturn);
	}

	async.series(callbacks, function(err, result) {
		if(typeof callback === 'function') {
			callback(result);
		}
	});
};

module.exports = NetatmoHistory;