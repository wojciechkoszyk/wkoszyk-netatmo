var querystring = require('querystring');
var https = require('https');
var crypto = require('crypto');

function NetatmoOauth(options) {
	this.client_id = options.nconf.get('netatmo').client_id;
	this.client_secret = options.nconf.get('netatmo').client_secret;
	this.credentials = {
		user: options.nconf.get('netatmo').user,
		password: options.nconf.get('netatmo').password
	};
	this.tokenRedirectUrl = options.nconf.get('server').desktop.url + '/getToken';
	this.mysql = options.mysql;
};

NetatmoOauth.prototype.getToken = function(options, callback) {
	var self = this;
	var state = options.state;
	var netatmoCode = options.code;

	// Build the post string from an object
	var post_data = querystring.stringify({
	    "grant_type": 'authorization_code',
	    "client_id": this.client_id,
	    "client_secret": this.client_secret,
	    "code": netatmoCode,
	    "redirect_uri": this.tokenRedirectUrl,
	    "scope": 'read_station'
	});

	// An object of options to indicate where to post to
	var post_options = {
	    host: 'api.netatmo.net',
	    port: '443',
	    path: '/oauth2/token',
	    method: 'POST',
	    headers: {
	        'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
	        'Content-Length': Buffer.byteLength(post_data)
	    }
	};

	// Set up the request
	var post_req = https.request(post_options, function(res, err) {
	    var data = '';

	    res.setEncoding('utf8');
	    res.on('data', function (chunk) {
	        data += chunk;
	    });

	    res.on('end', function() {
	    	var saveData = JSON.parse(data);

	    	self.mysql.saveTokens({
	    	    access_token: saveData.access_token,
	    	    refresh_token: saveData.refresh_token
	    	}, function() {
	    		if(typeof callback === 'function') {
	    			callback(data);
	    		} else {
	    			console.log('no callback')
	    		}
	    	});

	        post_req.end();
	    });
	});

	// post the data
	post_req.write(post_data);
};

NetatmoOauth.prototype.refreshToken = function(callback) {
	var self = this;
	var tokens = this.mysql.getTokens();

	// Build the post string from an object
	var post_data = querystring.stringify({
	    "grant_type": 'refresh_token',
	    "client_id": this.client_id,
	    "client_secret": this.client_secret,
	    "refresh_token": tokens.refresh_token
	});

	// An object of options to indicate where to post to
	var post_options = {
	    host: 'api.netatmo.net',
	    port: '443',
	    path: '/oauth2/token',
	    method: 'POST',
	    headers: {
	        'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
	        'Content-Length': Buffer.byteLength(post_data)
	    }
	};

	// Set up the request
	var post_req = https.request(post_options, function(res, err) {
	    var data = '';

	    res.setEncoding('utf8');
	    res.on('data', function (chunk) {
	        data += chunk;
	    });

	    res.on('end', function() {
    		var saveData = JSON.parse(data);

    		self.mysql.saveTokens({
    		    access_token: saveData.access_token,
    		    refresh_token: saveData.refresh_token
    		}, function() {
    			if(typeof callback === 'function') {
    				callback(data);
    			} else {
    				console.log('no callback')
    			}
    		});

    	    post_req.end();
	    });
	});

	// post the data
	post_req.write(post_data);
};

NetatmoOauth.prototype.authorize = function(callback) {
	var host = 'https://api.netatmo.net/oauth2/authorize';
	var scope = 'read_station';
    var state = crypto.randomBytes(20).toString('hex');

	var url = host+'?client_id='+this.client_id+'&redirect_uri='+this.tokenRedirectUrl+'&scope='+scope+'&state='+state;
	
	if(typeof callback === 'function') {
		callback(url);
	}
};

NetatmoOauth.prototype.authorizeFromCredentials = function() {
	var self = this;

	// Build the post string from an object
	var post_data = querystring.stringify({
	    "grant_type": 'password',
	    "client_id": this.client_id,
	    "client_secret": this.client_secret,
	    "username": this.credentials.user,
	    "password": this.credentials.password
	});

	// An object of options to indicate where to post to
	var post_options = {
	    host: 'api.netatmo.net',
	    port: '443',
	    path: '/oauth2/token',
	    method: 'POST',
	    headers: {
	        'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
	        'Content-Length': Buffer.byteLength(post_data)
	    }
	};

	// Set up the request
	var post_req = https.request(post_options, function(res, err) {
	    var data = '';

	    res.setEncoding('utf8');
	    res.on('data', function (chunk) {
	        data += chunk;
	    });

	    res.on('end', function() {
    		var saveData = JSON.parse(data);

    		self.mysql.saveTokens({
    		    access_token: saveData.access_token,
    		    refresh_token: saveData.refresh_token
    		}, function() {
    			console.log("Saved tokens", saveData.access_token);
    			if(typeof callback === 'function') {
    				callback(data);
    			} else {
    				console.log('no callback')
    			}
    		});

    	    post_req.end();
	    });
	});

	// post the data
	post_req.write(post_data);
};

module.exports = NetatmoOauth;