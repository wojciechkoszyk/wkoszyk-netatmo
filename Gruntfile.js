/**
 * Skybet gruntfile
 * @author Wojciech Koszyk <wojciech.koszyk@grandparade.co.uk>
 */

module.exports = function(grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        // ### grunt-contrib-stylus
        // compile stylus to css
        stylus: {
            compile: {
                files: {
                    'static/css/compiled/widget.css': ['static/css/stylus/*.styl']
                }
            }
        },

        // ### grunt-autoprefixer
        // Autoprefix all the things, for the last 2 versions of major browsers
        autoprefixer: {
            options: {
                silent: true, // suppress logging
                map: false, // Use and update the sourcemap
                browsers: ['last 2 versions', '> 1%', 'Explorer 10']
            },
            all: {
                src: 'static/css/compiled/widget.css'
            }
        },

        // ### grunt-contrib-cssmin
        // Minify all the css
        cssmin: {
            options: {
                keepSpecialComments: '*',
                noAdvanced: true
            },
            prod: {
                src: [
                    'static/css/compiled/widget.css', 
                    'static/css/leaflet.css'
                ],
                dest: 'static/css/compiled/widget.min.css'
            },
        },

        // ### grunt-contrib-concat
        // concatenate multiple JS files into a single file ready for use
        concat: {
            all: {
                nonull: true,
                dest: 'static/js/compiled/widget.js',
                src: [
                    'static/js/vendors/leaflet.js',
                    'static/js/vendors/chroma.js',
                    'static/js/src/Helpers.js',
                    'static/js/src/NetatmoPublic.js'
                ]
            },
        },

        // ### grunt-contrib-uglify
        // Minify concatenated javascript files ready for production
        uglify: {
            all: {
                options: {
                    sourceMap: false
                },
                files: {
                    'static/js/compiled/widget.min.js': 'static/js/compiled/widget.js'
                }
            }
        },

        // ### grunt-contrib-watch
        // Development watch tasks
        watch: {
            stylus: {
                files: ['static/css/stylus/**/*.styl'],
                tasks: ['stylus:compile', 'autoprefixer:all', 'cssmin:prod']
            },
            js: {
                files: ['static/js/src/**/*.js'],
                tasks: ['concat:all']
            }
        }
    });

    // These plugins provide necessary tasks.
    require('load-grunt-tasks')(grunt, {
        scope: 'devDependencies'
    });
    require('time-grunt')(grunt);

    grunt.registerTask('default', []);
    grunt.registerTask('build', ['stylus:compile', 'autoprefixer:all', 'cssmin:prod', 'concat:all', 'uglify:all']);
};