var nconf = require('nconf');
nconf.argv().env();
nconf.file({
    file: __dirname + '/config.json'
});

var _ = require('lodash');

var http = require('http');
var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var session = require('express-session');

app.set('views', __dirname + '/views');

// Don't minify in development
if (app.get('env') === 'development') {
    app.locals.pretty = true;
}

app.use('/static', express.static(__dirname + '/static'));
app.use('/css', express.static(__dirname + '/static/css'));
app.use('/js', express.static(__dirname + '/static/js'));
app.use('/images', express.static(__dirname + '/static/images'));
app.use('/fonts', express.static(__dirname + '/static/fonts'));

// Create application/json parser
var jsonParser = bodyParser.text();

// netatmo modules init
var NetatmoOauth = require(__dirname + '/modules/NetatmoOauth.js');
var NetatmoApi = require(__dirname + '/modules/NetatmoApi.js');
var NetatmoSQL = require(__dirname + '/modules/NetatmoSQL.js');

var mysql = new NetatmoSQL({nconf: nconf});
var oauth = new NetatmoOauth({nconf: nconf, mysql: mysql});
var netatmoApi = new NetatmoApi({mysql: mysql, oauth: oauth});

oauth.authorizeFromCredentials();

// Say Hello to user on index route
app.get('/', function(req, res) {
    res.render('public.jade');
});

app.get('/public', function(req, res) {
    res.render('public.jade');
});

// app.post('/public/saveDevices', jsonParser, function(req, res) {
//     var dashboardRes = res;
//     var dashboardReq = req;

//     netatmoApi.getPublicDevices({
//         data: JSON.parse(dashboardReq.body),
//         onSuccess: function(data) {
//             console.log('getPublicDevices onSuccess')
//             dashboardRes.send('done');
//         },
//         onError: function(error) {
//             console.log('getPublicDevices onError')
//             oauth.authorizeFromCredentials();
//         }
//     });
// });

app.post('/public/get', jsonParser, function(req, res) {
    var bounds = JSON.parse(req.body);

    mysql.getBoundsDevices(bounds, function(result) {
        res.send(JSON.stringify(result));
    });
});

app.post('/climatograph/get', jsonParser, function(req, res) {
    var request = JSON.parse(req.body);

    var options = {
        deviceId: request.id,
        selectedYear: request.year
    };

    mysql.getClimatograph(options, function(result) {
        if(typeof result === 'undefined' || result.length === 0) {
            // getMeasure
            netatmoApi.getClimatograph(options, function(data) {
                if(data.error) {
                    res.status(400).send('Oh snap, some error.');
                } else {
                    console.log('*** RENDER FROM API', data.device_id, options.selectedYear)
                    res.render('climatograph.jade', {
                        data: data.climatographData,
                        readingsRange: data.readingsRange,
                        device_id: options.deviceId,
                        year: options.selectedYear
                    });
                }
            });
        } else {
            console.log('*** RENDER FROM SQL', options.deviceId, options.selectedYear)
            res.render('climatograph.jade', {
                data: JSON.parse(result[0].data),
                readingsRange: JSON.parse(result[0].range),
                city: result[0].city,
                country: result[0].country,
                device_id: options.deviceId,
                year: options.selectedYear
            });
        }
    });
});

app.post('/climatograph/update', jsonParser, function(req, res) {
    var request = JSON.parse(req.body);

    var options = {
        deviceId: request.id,
        selectedYear: request.year,
        update: true
    };

    // getMeasure
    netatmoApi.getClimatograph(options, function(data) {
        if(data.error) {
            res.status(400).send('Oh snap, some error.');
        } else {
            res.render('climatograph.jade', {
                data: data.climatographData,
                readingsRange: data.readingsRange,
                device_id: options.deviceId,
                year: options.selectedYear
            });
        }
    });
});

app.post('/climatograph/saveCity', jsonParser, function(req, res) {
    var data = JSON.parse(req.body);

    mysql.saveCity(data, function(data) {
        if(data.error) {
            res.status(200).send('{"saved":false');
        } else {
            res.status(200).send('{"saved":true}')
        }
    });
});

app.use(function(err, req, res, next) {
    console.log(err);
    return res.status(400).send('Oh snap, some error.');
});
app.listen(nconf.get('server').port, nconf.get('server').hostname, function() {
    console.log('Server running on '+nconf.get('server').hostname+':'+nconf.get('server').port);
});
