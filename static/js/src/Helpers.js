var WK = WK || {};

// it loads data using XHR call using GET
WK.loadURL = function(config) {
    var xhr;
    var method = config.method || 'GET';
    var data = config.data || {};

    if (typeof XMLHttpRequest !== 'undefined') {
        xhr = new XMLHttpRequest();
    } else {
        var versions = [
            "MSXML2.XmlHttp.5.0",
            "MSXML2.XmlHttp.4.0",
            "MSXML2.XmlHttp.3.0",
            "MSXML2.XmlHttp.2.0",
            "Microsoft.XmlHttp"
        ];

        for (var i = 0, len = versions.length; i < len; i++) {
            try {
                xhr = new ActiveXObject(versions[i]);
                break;
            } catch (e) {}
        }
    }

    xhr.onreadystatechange = ensureReadiness;

    function ensureReadiness() {
        if (xhr.readyState < 4) {
            return;
        }
        if (xhr.status !== 200) {
            if(typeof config.error === 'function') {
                config.error(xhr);
            } else {
                // console.log("XHR error", xhr);
                return;
            }
        }
        if (xhr.readyState === 4) {
            if(typeof config.success === 'function') {
                config.success(xhr);
            } else {
                // console.log("XHR success", xhr);
                return;
            }
        }
    }

    xhr.open(method, config.url, true);
    // xhr.setRequestHeader('Content-type', 'text/plain');
    xhr.send(data);
};

WK.encodeQueryData = function(data) {
    var ret = [];

    for(var d in data) {
        ret.push(encodeURIComponent(d) + "=" + encodeURIComponent(data[d]));
    }

    return ret.join("&");
};