var NetatmoPublic = function() {
	this.map = {};

	this.$climatographHeader = {};
	this.$climatographDataWrapper = {};
	this.currentPin = {
		lat: null,
		lon: null,
		id: null
	};
	this.gradientTemperature = chroma.scale(['0A68D3', '33AFE0', 'D2FF63', 'FFE928', 'EA6325', '8C0404']).domain([-50,0,5,10,15,50]);
	this.gradientPrecipitation = chroma.scale(['B7E2FF', '004E89']).domain([0,200]);
	this.staticPath = 'images/';
	this.defaultYear = 2015;
};

NetatmoPublic.prototype.init = function() {
	this.cacheElements();
	this.attachListeners();	

	this.$map.style.height = window.innerHeight + 'px';
	this.initMap();
};

NetatmoPublic.prototype.cacheElements = function() {
	this.$map = document.getElementById('map');
	this.$attributions = {
		$donate: document.getElementById('donate'),
		$about: document.getElementById('about')
	}
	this.$saveResult = document.getElementById('saveresult');
	this.$climatograph = document.getElementById('climatograph');
	this.$attributionButtons = document.querySelectorAll('.attribution-button');
	this.$closeButtons = document.querySelectorAll('.close-button');
};

NetatmoPublic.prototype.attachListeners = function() {
	var self = this;

	// markers click action
	this.$map.addEventListener('click', function(e) {
		if(e.target.classList.contains('leaflet-clickable')) {
			var data = e.target.dataset;
			self.currentPin = {
				lat: data.lat,
				lon: data.lon,
				id: data.id
			};
			self.enableLoading();
			self.getClimatograph(data, self.defaultYear);
		}
	});

	// update climatograph button
	this.$climatograph.addEventListener('click', function(e) {
		if(e.target.classList.contains('climatograph-update')) {
			self.$climatograph.classList.add('loading');
			self.updateClimatograph(e);
		}
	});

	// atrribution buttons
	for(var i=0; i<this.$attributionButtons.length; i++) {
		this.$attributionButtons[i].addEventListener('click', function(e) {
			var id = e.target.getAttribute('data-id');
			for(var j in self.$attributions) {
				if(j.substr(1,j.length) !== id) {
					self.$attributions[j].classList.remove('show');
				}
			}
			self.$attributions['$'+id].classList.toggle('show');
		});
	}

	// close buttons
	for(var i=0; i<this.$closeButtons.length; i++) {
		this.$closeButtons[i].addEventListener('click', function(e) {
			e.target.parentNode.classList.remove('show');
		});
	}

	this.$climatograph.addEventListener('change', function(e) {
		self.enableLoading();
		// TO DO validate(e.target.value)
		self.getClimatograph(self.currentPin, e.target.value);
	});
};

NetatmoPublic.prototype.initMap = function() {
	var self = this;
	this.map = L.map('map', {zoomControl: false}).setView([50.14, 5.4], 4);
	this.markers = L.layerGroup();
	
	new L.Control.Zoom({ position: 'bottomright' }).addTo(this.map);

	L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
	    attribution: 'Data &copy; <a href="http://netatmo.com">Netatmo</a> | Maps &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors',
	    minZoom: 3,
	    maxZoom: 10
	}).addTo(this.map);

	this.getDevices(this.map.getBounds());

	this.map.on('moveend', function(e) {
	   var bounds = self.map.getBounds();
	   self.getDevices(bounds);
	});

	this.RedMarker = L.Icon.Default.extend({
		options: {
			iconUrl: this.staticPath+'marker-icon-red.png',
			iconRetinaUrl: this.staticPath+'marker-icon-red-2x.png',
			shadowUrl: this.staticPath+'marker-shadow.png'
		}
	});

	this.BlueMarker = L.Icon.Default.extend({
		options: {
			iconUrl: this.staticPath+'marker-icon.png',
			iconRetinaUrl: this.staticPath+'marker-icon-2x.png',
			shadowUrl: this.staticPath+'marker-shadow.png'
		}
	});
};

// get devices from given bounds
NetatmoPublic.prototype.getDevices = function(bounds) {
	var self = this;

	bounds.zoom = this.map.getZoom();

	// zoom adjustment
	if(bounds.zoom > 4) {
		bounds.zoom = bounds.zoom - 1;
	}
	if(bounds.zoom < 4) {
		bounds.zoom = 4;
	}

	WK.loadURL({
		method: 'POST',
		url: '/public/get',
		data: JSON.stringify(bounds),
		success: function(res) {
			var devices = JSON.parse(res.response);
			self.renderMarkers(devices);
		}
	});
};

NetatmoPublic.prototype.renderMarkers = function(devices) {
	var self = this;

	var marker = {};
	var icon = {};
	var modulesLength = 0;

	self.markers.clearLayers();

	for(var i=0; i<devices.length; i++) {
		modulesLength = devices[i].modules.length;

		// determine icon
		if(modulesLength > 21) {
			icon = new this.RedMarker();
		} else {
			icon = new this.BlueMarker();
		}

		// create marker
		marker = L.marker([devices[i].lat, devices[i].lon], {icon: icon});
		marker._device_id = devices[i].id;

		self.markers.addLayer(marker);
	}

	self.map.addLayer(self.markers);

	var $el = {};
	for(var i in this.markers._layers) {
		$el = this.markers._layers[i];
		$el._icon.setAttribute('data-id', $el._device_id);
		$el._icon.setAttribute('data-lat', $el._latlng.lat);
		$el._icon.setAttribute('data-lon', $el._latlng.lng);
	}
};

NetatmoPublic.prototype.getClimatograph = function(data, year) {
	var self = this;
	var toSend = {
		id: data.id,
		year: year
	};

	WK.loadURL({
		method: 'POST',
		url: '/climatograph/get',
		data: JSON.stringify(toSend),
		success: function(res) {
			self.showClimatograph(res, data.id, year);
		},
		error: function() {
			self.$climatograph.innerHTML = 'An error occured. Please try again later.';
		}
	});
};

NetatmoPublic.prototype.updateClimatograph = function(e) {
	var self = this;


	var toSend = {
		id: e.target.getAttribute('data-id'),
		year: e.target.getAttribute('data-year')
	};

	WK.loadURL({
		method: 'POST',
		url: '/climatograph/update',
		data: JSON.stringify(toSend),
		success: function(res) {
			self.showClimatograph(res, toSend.id, toSend.year);
		},
		error: function() {
			self.$climatograph.innerHTML = 'An error occured. Please try again later.';
		}
	});
};

NetatmoPublic.prototype.showClimatograph = function(res, deviceId, year) {
	this.$climatograph.innerHTML = res.response;
	// console.log('*** SHOW CLIMATOGRAPH', deviceId, year)

	// handle error
	if(this.$climatograph.querySelector('.error')) {
		this.$climatograph.classList.remove('loading');
		return;
	}

	// set global climatograph options
	if(this.$climatograph.querySelector('.module-rain')) {
		this.$climatograph.classList.add('with-precipitation');
	} else {
		this.$climatograph.classList.remove('with-precipitation');
	}

	// set cells colours
	var tempCells = this.$climatograph.querySelector('.climatograph-temperature').querySelectorAll('td');
	var rainCells = this.$climatograph.querySelector('.climatograph-precipitation').querySelectorAll('td');

	for(var i=0; i<tempCells.length; i++) {
		if(!isNaN(parseInt(tempCells[i].innerHTML))) {
			var colour = this.gradientTemperature(parseInt(tempCells[i].innerHTML))._rgb;
			var colourString = 'rgb('+parseInt(colour[0])+','+parseInt(colour[1])+','+parseInt(colour[2])+')';
			tempCells[i].style.backgroundColor = colourString;
		}
	}

	for(var i=0; i<rainCells.length; i++) {
		if(!isNaN(parseInt(rainCells[i].innerHTML))) {
			var colour = this.gradientPrecipitation(parseInt(rainCells[i].innerHTML))._rgb;
			var colourString = 'rgb('+parseInt(colour[0])+','+parseInt(colour[1])+','+parseInt(colour[2])+')';
			rainCells[i].style.backgroundColor = colourString;
		}
	}

	// select correect option
	var selectOptions = this.$climatograph.querySelectorAll('option');
	for(var i=0; i<selectOptions.length; i++) {
		if(parseInt(selectOptions[i].value) === parseInt(year)) {
			selectOptions[i].setAttribute('selected', 'selected');
		}
	}

	// populate place location
	this.$climatographHeader = this.$climatograph.querySelector('.climatograph-title');

	if(this.$climatographHeader.innerHTML === '' || this.$climatographHeader.innerHTML.indexOf('undefined') !== -1 || this.$climatographHeader.innerHTML.indexOf('null') !== -1) {
		this.getCityName(this.currentPin.lat, this.currentPin.lon, deviceId);
	} else {
		this.$climatograph.classList.remove('loading');
	}
};

NetatmoPublic.prototype.getCityName = function(lat, lon, deviceId) {
	var self = this;
	var nominatimData = {
		format: 'json',
		lat: lat,
		lon: lon,
		zoom: 10,
		addressdetails: 1,
		'accept-language': 'en'
	};

	nominatimData = WK.encodeQueryData(nominatimData);

	WK.loadURL({
		method: 'GET',
		url: 'http://nominatim.openstreetmap.org/reverse?'+nominatimData,
		success: function(res) {
			var address = JSON.parse(res.response).address;
			// console.log(address);

			var city = '';
			if(address.city && address.city.indexOf('District of') === -1) {
				city = address.city;
			} else {
				city = address.city_district || address.town || address.village || address.county || address.hamlet || address.state_district || address.road;
			}

			self.$climatographHeader.innerHTML = 'Climate data for ' +city + ', ' + address.country;
			self.$climatograph.classList.remove('loading');

			self.saveCity(city, address.country, deviceId);
		}
	});
};

NetatmoPublic.prototype.saveCity = function(cityString, countryString, deviceId) {
	var self = this;

	var data = {
		city: cityString,
		country: countryString,
		deviceId: deviceId
	};

	WK.loadURL({
		method: 'POST',
		url: '/climatograph/saveCity',
		data: JSON.stringify(data),
		success: function(res) {
			// console.log('city saved', res)
		}
	});
};

NetatmoPublic.prototype.enableLoading = function() {
	this.$climatograph.classList.add('show');
	this.$climatograph.classList.add('loading');
};