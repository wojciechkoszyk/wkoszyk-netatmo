var NetatmoHistory = function() {};

NetatmoHistory.prototype.init = function() {
	this.cacheElements();
	this.attachListeners();	
};

NetatmoHistory.prototype.cacheElements = function() {
	this.$setDataForm = document.querySelector('#set-data-form');

	this.$availableData = document.querySelector('#available-data');
	this.$generateClimatographForm = document.querySelector('#generate-climatograph');
	// this.$setHistoryFrom = this.$setForm.querySelector('#set-history-from');
	// this.$setHistoryTo = this.$setForm.querySelector('#set-history-to');
	// this.$setDeviceID = this.$setForm.querySelector('#set-device-id');
	// this.$setModuleID = this.$setForm.querySelector('#set-module-id');
	// this.$setDeviceType = this.$setForm.querySelector('#set-device-type');

	// this.$getForm = document.querySelector('#get-history');

	// this.$getHistoryFrom = this.$getForm.querySelector('#get-history-from');
	// this.$getHistoryTo = this.$getForm.querySelector('#get-history-to');
	// this.$getModuleID = this.$getForm.querySelector('#get-module-id');

	// this.$climatograph = document.querySelector('#climatograph');
};

NetatmoHistory.prototype.attachListeners = function() {
	var self = this;

	// this.$setForm.addEventListener('submit', function(e) {
	// 	e.preventDefault();

	// 	var data = {
	// 		historyFrom: self.$setHistoryFrom.value,
	// 		historyTo: self.$setHistoryTo.value,
	// 		deviceID: self.$setDeviceID.value,
	// 		moduleID: self.$setModuleID.value,
	// 		deviceType: self.$setDeviceType.value
	// 	};

	// 	WK.loadURL({
	// 		method: 'POST',
	// 		url: '/setHistory',
	// 		data: JSON.stringify(data),
	// 		success: function(res) {
	// 			console.log('success', res.response);
	// 		}
	// 	});
	// });

	// this.$getForm.addEventListener('submit', function(e) {
	// 	e.preventDefault();

	// 	var data = {
	// 		historyFrom: self.$getHistoryFrom.value,
	// 		historyTo: self.$getHistoryTo.value,
	// 		moduleID: self.$getModuleID.value,
	// 	};

	// 	WK.loadURL({
	// 		method: 'POST',
	// 		url: '/getHistory',
	// 		data: JSON.stringify(data),
	// 		success: function(res) {
	// 			self.$climatograph.innerHTML = res.response;
	// 		}
	// 	});
	// });

	this.$setDataForm.addEventListener('submit', function(e) {
		e.preventDefault();

		var data = {
			month: e.target.elements.month.value,
			year: e.target.elements.year.value,
			device_id: e.target.elements.device_id.value,
		};

		WK.loadURL({
			method: 'POST',
			url: '/history/save',
			data: JSON.stringify(data),
			success: function(res) {
				console.log('saved');
			}
		});
	});

	this.$generateClimatographForm.addEventListener('submit', function(e) {
		e.preventDefault();
		var $devices = document.querySelectorAll('.wk-device');
		var data = ['02:00:00:12:44:76', '05:00:00:01:75:0c'];

		for(var i=0; i<$devices.length; i++) {
			data.push($devices[i].getAttribute('data-id'));
		}

		console.log(data);
		WK.loadURL({
			method: 'POST',
			url: '/climatograph/generate',
			data: JSON.stringify(data),
			success: function(res) {
				console.log(res.response);
			}
		});
	});

	window.onload = function() {
		var $devices = document.querySelectorAll('.wk-device');
		var data = [];

		for(var i=0; i<$devices.length; i++) {
			data.push($devices[i].getAttribute('data-id'));
		}

		WK.loadURL({
			method: 'POST',
			url: '/history/devices',
			data: JSON.stringify(data),
			success: function(res) {
				var devicesLength = 0;
				var data = JSON.parse(res.response);

				for(var i=0; i<data.length; i++) {
					if(data[i].id !== null) {
						var from = new Date(data[i].min*1000);
						var to = new Date(data[i].max*1000);
						var name = document.querySelector('[data-id="'+data[i].id+'"]').getAttribute('data-name');

						devicesLength++;
						self.$availableData.innerHTML += 
							'<li><span>'+name+'</span><ul>'+
								'<li>ID: <span>'+data[i].id+'</span></li>'+
								'<li>First measure: <span>'+from+'</span></li>'+
								'<li>Last measure: <span>'+to+'</span></li>'+
							'</ul></li>';
					}
				}

				if(devicesLength === 0) {
					self.$availableData.innerHTML += '<li>No data available</li>';
				}
			}
		});
	};
};

